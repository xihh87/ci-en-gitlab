---
title: "Integración Continua con Gitlab y mk"
author: "Joshua Haase"
bibliography: ci-en-gitlab.json
biblio-style: apa
biblio-title: Referencias
date: 2021-04-24 $\newline$ $\newline$ ![](./web/media.espora.org/mgoblin_media/media_entries/2493/03_FLISOL_-_RANCHO.png){width=3cm}
theme: metropolis
slide-level: 2
lang: es-mx
---

Desde hace mucho tiempo soñamos ~~y tememos~~ que los robots van a hacer nuestro trabajo
------------------------------------------------------------------------------------

![](web/live.staticflickr.com/7101/13193883615_f80917b84b_b.jpg "https://www.flickr.com/photos/chryslergroup/13193883615/ CC-BY-NC-SA")

Desde hace mucho tiempo ~~soñamos~~ y tememos que los robots van a hacer nuestro trabajo
------------------------------------------------------------------------------------

![La tecnología destruye trabajos, pero generalmente surgen nuevos trabajos para reemplazarlos, alguien tiene que mantener funcionando a los robots](web/res.cloudinary.com/people-matters/image/upload/w_624,h_351,c_scale,q_auto,f_auto/v1477030606/1477030605.jpg "https://www.peoplematters.in/article/hr-technology/how-to-buy-hr-tech-payroll-leave-attendance-system-part-1-14381 CC-NC-SA"){width=8cm}

La tecnología nos permite hacer más con menos esfuerzo
------------------------------------------------------

![](web/i0.wp.com/clipset.20minutos.es/wp-content/uploads/2019/01/robots-work.jpg "https://clipset.com/linkedin-profesiones-trabajos-futuro-2019/ CC-BY-NC-SA")

Y se supone que entonces las personas nos podemos enfocar a las cosas más valiosas
----------------------------------------------------------------------------------

[![](web/about.gitlab.com/video/autodevops-animation/autodevops-animation.png)](web/about.gitlab.com/video/autodevops-animation/autodevops-animation.mp4 )

Integración Continua es consolidar el trabajo de las personas tan constantemente como sea posible
-------------------------------------------------------------------------------------------------

![La imagen de moda para vender DevOps, CI/CD](web/cdn-images-1.medium.com/max/1600/1*TNJ7Rpr5G1OJHtKH-IBEFw.png)

Despliegue Continuo es la capacidad de publicar automáticamente paquetes con el código
--------------------------------------------------------------------------------------

![](web/community-cdn-digitalocean-com.global.ssl.fastly.net/assets/tutorials/images/large/Package_Management_tw_mostov.png "https://www.digitalocean.com/community/tutorials/package-management-basics-apt-yum-dnf-pkg CC-BY-NC-SA")

Son prácticas para mejorar la colaboración en un equipo de trabajo
------------------------------------------------------------------

::: incremental
::::: {.columns}
:::: {.column width=50%}
![](web/cdn.80000hours.org/wp-content/uploads/2015/06/software-engineering-test.jpg )
::::
:::: {.column width=50%}
-   El desarrollo de software es un trabajo social. $\newline$

-   Los sistemas son tan complejos que nadie los conoce en su totalidad. $\newline$

-   Para que sean útiles deben poder usarse por personas en contextos diferentes.
::::
:::::
:::


Cómo vamos a automatizar usando ![](web/about.gitlab.com/images/press/logo/jpg/gitlab-logo-gray-stacked-rgb.jpg){width=3cm}
======================================

Si queremos que algo funcione «en la nube» debemos poder hacerlo funcionar en nuestra computadora
-------------------------------------------------------------------------------------------------

![](web/1.bp.blogspot.com/-wl-z36-WKZQ/XKgUqyz64bI/AAAAAAAAG94/EL8ukfhm86sqYhfRNRCffftOa-ra3m19ACLcBGAs/s1600/thereisnocloud-v2.png ){width=7cm}


Conviene empezar los proyectos automatizando el entorno de trabajo
------------------------------------------------------------------

::: incremental
1.  Evita la excusa «Funciona en mi computadora».

2.  Facilita colaborar en el proyecto.

3.  Hace más explícita la gestión de dependencias.
:::

Para automatizar un entorno de trabajo, usaremos docker
-------------------------------------------------------
