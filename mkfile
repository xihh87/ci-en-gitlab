FORMAT=markdown+grid_tables+table_captions+pipe_tables+example_lists+fenced_code_attributes+fenced_divs+bracketed_spans+footnotes
APA7=https://raw.githubusercontent.com/citation-style-language/styles/master/apa.csl

help:QV: ## show this command
	cmd/show-help mkfile \
	| sort

%.pdf:Q:	%.md
	pandoc \
		-f ${FORMAT} \
		--to beamer \
		--slide-level=2 \
		--pdf-engine=lualatex \
		--citeproc \
		-o ${target} \
		${prereq}
